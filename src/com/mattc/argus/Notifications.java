  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

package com.mattc.argus;

import java.awt.event.ActionEvent;   
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import com.mattc.argus.util.Console;
import com.mattc.argus.util.Utility;

/**
 * A Utility Class for displaying various Alert Dialogs.
 * 
 * @author Matthew Crocco
 *
 */
public class Notifications {
	
	private static JFrame running;
	private static SimpleDateFormat format;
	private volatile static JTextArea log;
	private volatile static JButton awesome;
	private volatile static JLabel runLabel;
	
	private static Dimension frameMin = new Dimension(500, 500);
	private static Dimension frameDef = new Dimension(700, 650);
	private static Dimension frameMax = new Dimension(2000, 750);
	
	/**
	 * Initializes All Progress Window Components and Layout
	 */
	public static synchronized final void init(){
		Font font = new Font("Serif", Font.BOLD, 21);
		Font cpyright = new Font("Monospace", Font.PLAIN, 12);
		JPanel cPanel = new JPanel();
		cPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		awesome = new JButton("Please Wait...");
		format = new SimpleDateFormat("HH:mm:ss");
		
		running = new JFrame(Ref.TITLE + " " + Ref.VERSION);
		running.getContentPane().setLayout(new BoxLayout(running.getContentPane(), BoxLayout.Y_AXIS));
		
		awesome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				running.dispose();
			}
		});
		
		runLabel = new JLabel("Installation In Progress...");
		JLabel logLabel = new JLabel("Log Snippet (Actual Log Found at Jar Directory as EInstall.log):");
		JLabel copyright = new JLabel("Matthew Crocco \u00A9 2014 -- (Class of 2015)");
		runLabel.setFont(font);
		copyright.setFont(cpyright);
		runLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		logLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		copyright.setAlignmentX(Component.LEFT_ALIGNMENT);
		cPanel.add(copyright);
		awesome.setAlignmentX(Component.CENTER_ALIGNMENT);
		running.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		running.setMaximumSize(frameMax);
		running.setSize(frameDef);
		running.setMinimumSize(frameMin);
		running.setLocationRelativeTo(null);
		running.setResizable(false);
		running.getContentPane().add(Box.createRigidArea(new Dimension(0, 20)));
		running.getContentPane().add(runLabel);
		running.getContentPane().add(Box.createRigidArea(new Dimension(0, 20)));
		running.getContentPane().add(logLabel);
		running.getContentPane().add(Box.createRigidArea(new Dimension(0, 5)));
		
		//LOG AREA
		JPanel sub = new JPanel();
		sub.setPreferredSize(new Dimension(700, 550));
		sub.setLayout(new BoxLayout(sub, BoxLayout.X_AXIS));
		sub.add(Box.createRigidArea(new Dimension(25, 0)));
		sub.add(makeScrollPane());
		sub.add(Box.createRigidArea(new Dimension(25, 0)));
		running.getContentPane().add(sub);
		//--
		
		running.getContentPane().add(Box.createRigidArea(new Dimension(0, 15)));
		running.getContentPane().add(awesome);
		running.getContentPane().add(Box.createRigidArea(new Dimension(0, 15)));
		running.getContentPane().add(cPanel);
		running.getContentPane().add(Box.createRigidArea(new Dimension(0, 20)));
		awesome.setEnabled(false);
		awesome.setPreferredSize(new Dimension(400, 50));
		running.setVisible(false);
		Console.info("GUI Initialized...");
	}
	
	/**
	 * Makes the Progress GUI Visible
	 */
	public static void displayInProgress(){
		running.setResizable(true);
		running.setVisible(true);
		
		Console.info("GUI Displayed! Awaiting Finalization...");
	}
	
	/**
	 * Indicates a Completed Installation, Modifies GUI Components and Allows Exiting
	 */
	public static void switchToFinished(){
		if(!running.isVisible()) throw new IllegalStateException("Progress Window Not Shown!");
		
		try{
			Thread.sleep(1500);
		}catch(InterruptedException e){}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				awesome.setText("Awesome! (Close)");
				runLabel.setText("Installation Complete!");
				awesome.setEnabled(true);
				running.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				running.setAlwaysOnTop(true);
				Console.debug("GUI Modified To Allow Closing...");
			}
		});
	}
	
	/**
	 * Allows up to 50 lines of Log Information (Memory Conservation) <br />
	 * This should NOT Be relied upon for Debug Information! <br />
	 * <br />
	 * All REAL Information Should be Printed to the Console or some Logging Utility. NOT Here!<br />
	 * Generally this is used to let the User know the program is still working and has not crashed and when it <i>has</i> crashed.
	 * @param msg
	 */
	public static void updateLog(final String initial){
		if(log == null || !log.isEnabled()){
			Console.warn("Attempt to modify 'In Progress Log' when it is Null/Unavailable!");
			return;
		}
		
		String msg = (log.getText().trim().length() > 0 ? "\n": "") + String.format("(%s)[%s]: %s", format.format(new Date()), Thread.currentThread().getName(), initial);
		log.append(msg);
				
		String current = log.getText().trim();

		if(Utility.getLines(current) > 60){
			while(Utility.getLines(current) > 60)
				current = current.substring(current.indexOf("\n") + 2);
			
			log.setText(current);
		}
	}

	private static JScrollPane makeScrollPane(){
		log = new JTextArea();
		log.setEditable(false);
		log.setFont(new Font("Monospaced", Font.PLAIN, 14));
		log.setBackground(Color.BLACK);
		log.setForeground(Color.WHITE);
		log.setLineWrap(true);
		((DefaultCaret) log.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		JScrollPane scroll = new JScrollPane(log);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		scroll.setPreferredSize(new Dimension(300, 300));
		return scroll;
	}
	
	/**Displays an Alert Message to the User*/
	public static final void alert(String msg){
		JOptionPane.showMessageDialog(Notifications.getCurrentProgressWindow(), msg, "EclipseInstaller Alert", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**Displays a Warning Message to the User*/
	public static final void warn(String msg){
		JOptionPane.showMessageDialog(Notifications.getCurrentProgressWindow(), msg, "EclipseInstaller Warning", JOptionPane.WARNING_MESSAGE);
	}
	
	/**Displays an Error Message to the User */
	public static final void error(String msg){
		JOptionPane.showMessageDialog(Notifications.getCurrentProgressWindow(), msg, "EclipseInstaller Error", JOptionPane.ERROR_MESSAGE);
	}
	
	/**Prints an Exception with no Information Message to the User */
	public static final void exception(Exception e){
		Notifications.exception(null, e);
	}
	
	/**Prints an Exception with Information to the User */
	public static final void exception(String info, Exception e){
		String title = "EInstall Exception!: " + e.getClass().getName();
		String msg = (info == null || info.trim() == "" ? "" : info + "\n\n");
		msg += e.getLocalizedMessage() + "\n";
		
		StackTraceElement[] elements = e.getStackTrace();
		for(StackTraceElement element: elements)
			msg += "\t" + element.toString() + "\n";
		
		msg += "\n\n";
		msg += "If you feel this is an issue or bug, report it to the following: \n";
		msg += "Developer Facebook:  \tMatthew Crocco\n";
		msg += "Developer E-Mail:    \tmatthewcrocco@gmail.com\n";
		msg += "EInstaller GitHub:   \thttps://github.com/Matt529/EclipseInstaller/issues\n";
		
		JOptionPane.showMessageDialog(getCurrentProgressWindow(), msg, title, JOptionPane.ERROR_MESSAGE);
	}
	
	/**Returns the Current Progress Window in Use*/
	public static final JFrame getCurrentProgressWindow(){
		return running;
	}
}
