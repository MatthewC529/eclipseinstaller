  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */


package com.mattc.argus.util;

import java.io.File; 
import java.io.IOException;

import org.apache.log4j.BasicConfigurator; 
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.Priority;
import org.apache.log4j.RollingFileAppender;

import com.mattc.argus.Notifications;
import com.mattc.argus.Ref;

public final class Console{

	@SuppressWarnings("deprecation")
	public enum LogLevel{
		INFO(Priority.INFO),
		DEBUG(Priority.DEBUG),
		ERROR(Priority.ERROR),
		FATAL(Priority.FATAL),
		WARN(Priority.WARN);
		
		public final Priority level;
		
		private LogLevel(Priority p){
			level = p;
		}
	}
	
	public static Logger logger;
	
	public static final String LOG_PATTERN = "%d{HH:mm:ss} - [%t][%-5p]{L:%-10c}: %m%n";
	
	static{
		File log = new File(Ref.getJarDir(), "EInstall.log");
		
		try{
			log.createNewFile();
			BasicConfigurator.configure(new ConsoleAppender(new PatternLayout(LOG_PATTERN)));
			BasicConfigurator.configure(new RollingFileAppender(new PatternLayout(LOG_PATTERN), log.getPath(), false));
		}catch(IOException e){
			e.printStackTrace();
		}
		logger = Logger.getLogger("EclipseInstall");
		logger.setLevel(Level.ALL);
	}
	
	public static void log(Object msg, Throwable t, LogLevel priority){
		logger.log(priority.level, msg, t);
	}
	
	public static void log(Object msg, LogLevel priority){
		logger.log(priority.level, msg);
	}
	
	public static void info(Object msg){
		logger.info(msg);
	}
	
	public static void debug(Object msg){
		logger.debug(msg);
	}
	
	public static void warn(Object msg){
		logger.warn(msg);
	}
	
	public static void error(Object msg){
		logger.error(msg);
	}
	
	public static void fatal(Object msg){
		logger.fatal(msg);
	}
	
	public static void exception(Exception e){
		StackTraceElement[] elements = e.getStackTrace();
		Notifications.updateLog("---!!An Exception of Type " + e.getClass().getSimpleName() + " was caught!!---");
		logger.error("========EXCEPTION========");
		logger.error("");
		logger.error(String.format("MESSAGE (T:%s): %s", e.getClass().getSimpleName(), e.getLocalizedMessage()));
		for(StackTraceElement ex: elements){
			logger.error("\t"+ex.toString());
		}
		logger.error("");
		logger.error("========EXCEPTION========");
	}
	
}
