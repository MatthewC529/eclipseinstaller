  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */


package com.mattc.argus.util;

import java.io.Closeable; 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Various Miscellaneous Utilities
 * @author Matthew Crocco
 *
 */
public final class Utility {
	
	private Utility(){}
	
	/**
	 * Small Utility to Determine OS and OS Architecture based on an enumeration <br />
	 * of supported OS's and Architectures.
	 * 
	 * @author Matthew Crocco
	 */
	public enum OS{
		
		WINDOWS(".exe"),
		UNIX(""),
		UNSUPPORTED("");
		
		public final String suffix;
		public static final byte BIT_64 = 0x40;
		public static final byte BIT_32 = 0x20;
		
		private OS(String executable){
			this.suffix = executable;
		}
		
		/**
		 * Get this System OS
		 * @return
		 */
		public static OS get(){
			String name = System.getProperty("os.name").toLowerCase();
			
			if(name.indexOf("win") >= 0)
				return WINDOWS;
			else if(name.indexOf("nix") >= 0 || name.indexOf("nux") >= 0 || name.indexOf("nax") >= 0)
				return UNIX;
			else
				return UNSUPPORTED;
		}
		
		/**
		 * Get Architecture. At the moment this is either 64-bit or 32-bit. <br />
		 * To distinguish the two use OS.BIT_32 and OS.BIT_64 for conditionals.
		 * @return
		 */
		public static byte getArch(){
			String arch = System.getProperty("os.arch");
			
			if(arch.indexOf("64") > 0)
				return OS.BIT_64;
			else
				return OS.BIT_32;
		}
	}
	
	/**
	 * Reflectively grab's a field from an Object of the descriptor == name. <br />
	 * <br />
	 * This Method is Synchronized to Prevent Multiple Class Grabs.
	 * 
	 * @param o
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	public static final <T> T grabField(Object o, String name) throws IllegalArgumentException, IllegalAccessException{
		synchronized(o){
			Field[] fields = o.getClass().getDeclaredFields();
			
			for(Field f: fields){
				if(f.getName().equals(name)){
					if(!f.isAccessible()){
						f.setAccessible(true);
						T val = (T) f.get(o);
						f.setAccessible(false);
						return val;
					}else{
						return (T) f.get(o);
					}
				}
			}
			
			return null;
		}
	}
	
	/**
	 * Returns the Number of Lines in a Given string using Regular Expressions
	 * @param str
	 * @return
	 */
	public static final int getLines(String str){
		String[] lines = str.split("\r\n|\r|\n");
		return lines.length;
	}
	
	/**
	 * Recursively Empties then Deletes a Folder. <br />
	 * <br />
	 * The boolean this method returns depends on whether or not the Directory passed is deleted. <br />
	 * If the directory can not be deleted then it returns false, though some of its contents may have been deleted anyway. <br />
	 * @param directory
	 * @return Whether or Not the Directory passed was Deleted
	 */
	public static final boolean deleteDirectory(File directory){
		if(!directory.isDirectory())
			throw new IllegalArgumentException(directory.getAbsolutePath() + " is NOT a Directory!");
		
		File[] entries = directory.listFiles();
		
		for(File file: entries){
			if(file.isDirectory())
				deleteDirectory(file);
			else{
				file.delete();
			}
		}
		
		return directory.delete();
	}
	
	/**
	 * Copy Src File to Dest, if Dest Exists it will be Overwritten
	 * @param src
	 * @param dest
	 * @return Success/Failure
	 */
	public static final boolean copyFile(File src, File dest){
		FileInputStream fis = null;
		FileOutputStream fos = null;
		boolean success = true;
		
		if(!src.exists()) throw new IllegalStateException("Source File does Not Exist!");
		if(!dest.exists()){
			try{
				dest.getParentFile().mkdirs();							//Ensure Trunk of Path Exists
				dest.createNewFile();									//Create File
			}catch(IOException e){
				Console.exception(e);
			}
		}
		
		
		try{
			byte[] buffer = new byte[8192];					
			fis = new FileInputStream(src);
			fos = new FileOutputStream(dest);	
			
			for(int c = fis.read(buffer); c > 0; c = fis.read(buffer))
				fos.write(buffer, 0, c);
		}catch(IOException e){
			e.printStackTrace();
			success = false;
		}finally{
			closeStream(fis);
			closeStream(fos);
		}
		
		return success;
	}
	
	/**
	 * Closes any Closeable, meant for Streams to clean up finally blocks.
	 * @param stream
	 */
	public static final void closeStream(Closeable stream){
		if(stream == null)
			return;
		
		try{
			stream.close();
		}catch(Exception e){}
	}
	
	/**
	 * Takes Two Path's and Relativizes One to the Other. In terms of parameters <br />
	 * the <code>'from'</code> path is made relative to the <code>'to'</code> path. <br />
	 * <br />
	 * If <code> 'to'</code> is not found in <code>'from'</code> then <code>'from'</code> is returned. <br/>
	 * <br />
	 * The Following Code will print "./Matt" <br />
	 * <code> <br />
	 * String from = "C://Users/Matt"; <br />
	 * String to = "C://Users"; <br />
	 * <br />
	 * System.out.println(relativizePath(from, to)); <br />
	 * </code> <br />
	 * <br />
	 * @param from
	 * @param to
	 * @return
	 */
	public static final String relativizePath(File from, File to){
		return relativizePath(from.getAbsolutePath(), to.getAbsolutePath());
	}
	
	/**
	 * Takes Two Path's and Relativizes One to the Other. In terms of parameters <br />
	 * the <code>'from'</code> path is made relative to the <code>'to'</code> path. <br />
	 * <br />
	 * If <code> 'to'</code> is not found in <code>'from'</code> then <code>'from'</code> is returned. <br/>
	 * <br />
	 * The Following Code will print "./Matt" <br />
	 * <code> <br />
	 * String from = "C://Users/Matt"; <br />
	 * String to = "C://Users"; <br />
	 * <br />
	 * System.out.println(relativizePath(from, to)); <br />
	 * </code> <br />
	 * <br />
	 * @param from
	 * @param to
	 * @return
	 */
	public static final String relativizePath(String from, String to){
		if(from.contains(to)){
			String tmp = from.substring(to.length());
			return "." + tmp;
		}else return from;
	}
	
	public static final String combinePaths(String root, String... more){
		StringBuilder sb = new StringBuilder(root);
		String sep = File.separator;
		
		if(root.endsWith("/") || root.endsWith("\\")){
			sb.deleteCharAt(sb.length()-1);
		}
		
		for(String s: more){
			sb.append(sep + s);
		}
		
		return sb.toString();
	}
	
}
