  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

package com.mattc.argus;

import java.io.File;

/**
 * Main Reference File
 * 
 * @author Matthew Crocco
 */
public final class Ref {
	
	private Ref(){} 
	
	public static final String TITLE = "Argus Installer";
	public static final String AUTHOR = "Matthew Crocco";
	public static final String VERSION = "v1.1.0a";
	
	/**Are we in DEBUG Mode? Should only be in Debug if in Eclipse!*/
	public static final boolean DEBUG = false;
	
	private static final File JAR_DIR = new File(".");
	private static final File DESKTOP_DIR = new File(System.getProperty("user.home"), "Desktop");
	
	/**Returns the Directory Path where the Code Exists*/
	public static final String getJarPath(){
		if(JAR_DIR.getAbsolutePath().contains("EclipseInstaller/bin/")){
			return "./";
		}else
			return JAR_DIR.getAbsolutePath();
	}
	
	/**Returns the Directory File where the Code exists*/
	public static final File getJarDir(){
		return JAR_DIR;
	}
	
	/**Returns the Directory Path of the Desktop*/
	public static final String getDesktopPath(){
		return DESKTOP_DIR.getAbsolutePath();
	}
	
	/**Returns the Directory File of the Desktop*/
	public static final File getDesktopDir(){
		return DESKTOP_DIR;
	}

	/**Default Config Contents, should the Config files need to be generated*/
	public static final String DEF_CFG = "# LISTS ALL ZIPS TO DECOMPRESS\n" +
										 "# The Threads Use A LOT Of Buffer Space to be Rapid\n" +
										 "# Each Thread Reserves 8192 Bytes of Memory for buffering, with the Default 3 that is 24 MiB of Memory Guaranteed To Be Used\n" +
										 "# \n" +
										 "# Be Wary of Adding Files, Especially Ones Greater Than 3 GB\n" +
										 "# There is No Limit but the recommended is at MOST 12 Large Zip Files...\n" +
										 "# \n" +
										 "# Only ZIP Compressed files work. Any other formats are ignored even if on this list. \n" +
										 "# Do Not Attempt GZIP, Tar Balls (tar.gz/tar.bz), RAR (WinRAR Compressed), etc. \n" +
										 "# \n" +
										 "# If this file IS deleted, then it defaults to accepting only the default 3 Zip Files \n" +
										 "# Do Not Delete This File!\n" +
										 "# \n" +
										 "# If It Is Missing, It Will Be Generated At Runtime\n" +
										 "\n" +
										 "eclipse-sdk $R: eclipse.ini\n" +
										 "Java\n" +
										 "Sublime Text\n";	
	
	/**Default Content for perms.cfg */
	public static final String DEF_PERMS = "#Lists All Files to Apply POSIX Permissions to (a+x)\n" +
										   "#'.' indicates relative to the destination directory\n" +
										   "#Leave out the '.' to indicate an absolute path!\n\n" +
										   "./eclipse-sdk/eclipse\n" +
										   "./Java/bin/java\n" +
										   "./Sublime Text/Sublime Text/sublime_text\n" +
										   "./Sublime Text/Sublime Text/plugin_host";
	
}
