  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

package com.mattc.argus.replace;

import java.io.BufferedReader;  
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import com.mattc.argus.Notifications;
import com.mattc.argus.util.Console;
import com.mattc.argus.util.Utility;
import com.mattc.argus.util.Utility.OS;

/**
 * Hardcoded Eclipse Replacement Handler. <br />
 * <br />
 * Because most schools restrict Java Version and updating is nigh impossible <br />
 * Eclipse Installer provides a portable JDK 1.7.0u60 that it installs and attaches to <br />
 * a generated eclipse.ini that then replaces the one provided in eclipse-sdk.zip. <br />
 * 
 * @author Matthew Crocco
 */
public final class Eclipse {

	//Technically you can instantiate it... but there is no point.
	private Eclipse(){}
	
	/**
	 * Grabs Text from Template Eclipse.ini and generates the Path to the Portable JVM
	 * @param dest
	 * @return
	 */
	public static final String getModifiedEclipseINI(File dest){
		char[] buffer = null;
		Writer writer = null;
		Reader reader = null;
		String contents = null;
		String[] bits = null;
		String result = "";
		OS system = OS.get();
		
		if(system == OS.UNSUPPORTED){
			Notifications.error("Unsupported OS Detected! Install Will Still Continue as if OS was Unix-Based...");
			system = OS.UNIX;
		}
		
		Console.debug("eclipse-" + system.name() + OS.getArch() +".ini");
		try{
			writer = new StringWriter();
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(new File(".", "ini"),"eclipse-" + system.name() + OS.getArch() + ".ini"))));
			buffer = new char[1024];
			
			for(int c = reader.read(buffer) ; c > 0; c = reader.read(buffer))
				writer.write(buffer, 0, c);
			
			contents = writer.toString();
			writer.close();
			reader.close();
		}catch(IOException e){
			Console.exception(e);
		}finally{
			Utility.closeStream(writer);
			Utility.closeStream(reader);
		}
		
		if(contents != null){
			String sep = System.getProperty("file.separator");
			File javaw = new File(dest, "Java" + sep + "bin");
			bits = contents.split("\r\n|\r|\n");
			
			for(String s: bits){
				result += s + System.getProperty("line.separator");
			}
			
			result = String.format(result, javaw.getAbsolutePath());
		}
		
		return result;
	}
	
	/**
	 * Generates the Eclipse.ini Replace File
	 * @param dest
	 */
	public static final void generateEclipseReplace(File dest){
		File dir = new File(".","replace");
		File ini = new File(dir, "eclipse.ini");
		ByteArrayInputStream bis = null;
		FileOutputStream fos = null;
		
		if(!dir.exists())
			dir.mkdirs();
		
		try{
			byte[] buffer = new byte[8192];
			ini.createNewFile();
			bis = new ByteArrayInputStream(Eclipse.getModifiedEclipseINI(dest).getBytes());
			fos = new FileOutputStream(ini);
			
			for(int c = bis.read(buffer); c > 0; c = bis.read(buffer))
				fos.write(buffer, 0, c);
		}catch(IOException e){
			Console.exception(e);
		}finally{
			Utility.closeStream(bis);
			Utility.closeStream(fos);
		}
		
	}
}
