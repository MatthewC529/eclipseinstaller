  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */


package com.mattc.argus.config;

import com.mattc.argus.util.Console;

public class ConfigException extends RuntimeException{

	private static final long serialVersionUID = 8900709087656399625L;

	public enum CEType{
		GENERATION("Failure to Generate Config File!"),
		READ("Failure to Read From Config File!"),
		WRITE("Failure to Write To Config File!"),
		READ_WRITE("Failure to do any I/O Operation with File!");
		
		public final String message;
		
		private CEType(String msg){
			message = msg;
		}
		
	}
	
	public ConfigException(){
		super("Unspecified Exception Occurred!");
		Console.exception(this);
	}
	
	public ConfigException(CEType exc){
		super(exc.message);
		Console.exception(this);
	}
	
}
