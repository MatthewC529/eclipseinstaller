  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

package com.mattc.argus.config;

import java.io.File;
import java.io.FileFilter;

/**
 * A Default Filter that only grabs the 3 Default Zip Files. <br />
 * <ul>
 * <li> Eclipse Luna Installation (eclipse-sdk)</li>
 * <li> JRE 7 (Java) </li>
 * <li> Sublime Text Editor v3 (Sublime Text) </li>
 * </ul>
 * @author Matthew Crocco
 *
 */
public class DefaultFilter implements FileFilter{

	public boolean accept(File f){
		String name = f.getAbsolutePath();
		if(name.contains("eclipse-sdk") || name.contains("Java") || name.contains("Sublime Text")){
			if(f.getAbsolutePath().endsWith(".zip")){
				return true;
			}
		}
		
		return false;
	}
	
}
