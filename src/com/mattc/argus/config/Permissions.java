package com.mattc.argus.config;

import java.io.ByteArrayInputStream; 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import com.mattc.argus.Notifications;
import com.mattc.argus.Ref;
import com.mattc.argus.util.Console;
import com.mattc.argus.util.Utility;
 
/**
 * A Class Representing a Permissions File
 * 
 * @author Matthew Crocco
 */
public class Permissions extends File{
	
	private static final long serialVersionUID = -6606787411057679512L;
	
	private boolean failed = false;
	private Set<String> paths = new HashSet<String>();
	
	public Permissions(){
		this("perms.cfg");
	}
	
	public Permissions(String path) {
		super(Ref.getJarDir(), path);
		
		if(!this.exists()){
			try {
				this.createNewFile();
				writeDefaults();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		parse();
	}
	
	public void applyToAll(File dest){
		if(failed) return;
		
		for(String path: paths){
			try {
				if(path.startsWith(".")){
					String[] parts = path.substring(path.indexOf(File.separator) + 1).split(File.separator);
					
					apply(Utility.combinePaths(dest.getAbsolutePath(), parts));
				}else 
					apply(path);
			} catch (IOException e) {
				Console.exception(e);
			}
		}
	}
	
	public static void apply(String path) throws IOException{
		if(new File("/usr/bin/chmod").exists()){
			Runtime.getRuntime().exec(new String[]{"/usr/bin/chmod", "+x", path});
			Console.debug("POSIX a+x Permission set for " + path + " using /usr/bin/chmod");
		}else if(new File("/bin/chmod").exists()){
			Runtime.getRuntime().exec(new String[]{"/bin/chmod", "+x", path});
			Console.debug("POSIX a+x Permission set for " + path + " using /bin/chmod");
		}else{
			Runtime.getRuntime().exec(new String[]{"chmod", "+x", path});
			Console.debug("POSIX a+x Permission set for " + path + " using chmod");
		}
	}
	
	private void parse(){
		try{
			Scanner scan = new Scanner(this);
			
			while(scan.hasNextLine()){
				String line = scan.nextLine();

				if("".equals(line.trim()) || line.startsWith("#")) continue;				
				paths.add(line);
			}
			
			scan.close();
		}catch(IOException e){
			Notifications.alert("Failure to Parse Permissions! POSIX Permissions WILL NOT be set!");
			Console.exception(e);
			failed = true;
		}
	}
	
	private void writeDefaults(){
		FileOutputStream fos = null;
		ByteArrayInputStream bis = null;
		
		try{
			byte[] buf = new byte[4096];
			fos = new FileOutputStream(this);
			bis = new ByteArrayInputStream(Ref.DEF_PERMS.getBytes());
			
			for(int c = bis.read(buf); c >= 0; c = bis.read(buf))
				fos.write(buf, 0, c);
		}catch(IOException e){
			Console.exception(e);
		}finally{
			Utility.closeStream(fos);
			Utility.closeStream(bis);
		}
	}
	
}
