  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */


package com.mattc.argus.config;

import java.io.File;
import java.io.FileFilter;

/**
 * A File Filter that Filters Zip Files to populate the Filenames List in Config
 * 
 * @author Matthew Crocco
 * @see com.mattc.argus.config.Config#parseInfo() Config.parseInfo()
 */
public class ConfigFilter implements FileFilter {

	private Config file;
	
	public ConfigFilter(Config file){
		this.file = file;
	}

	public boolean accept(File f) {
		String tmp = f.getName();
		if(tmp.indexOf('.') <= 0) return false;
		return file.filenames.contains(tmp.substring(0, tmp.indexOf('.'))) 
			   && 
			   tmp.endsWith(".zip");
	}
	
}
