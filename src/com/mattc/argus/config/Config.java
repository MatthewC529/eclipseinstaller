  /*Argus -- A Zip Installer for Circumventing Common Educational Web Blocks
    Copyright (C) 2014 Matthew Crocco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

package com.mattc.argus.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.mattc.argus.LaunchPoint;
import com.mattc.argus.Ref;
import com.mattc.argus.config.ConfigException.CEType;
import com.mattc.argus.util.Console;
import com.mattc.argus.util.Utility;

/**
 * Represents a Config File describing the Archive Files being extracted and any other <br />
 * Information.
 * 
 * @author Matthew Crocco
 *
 */
public final class Config extends File {
	
	//Serializable UID
	private static final long serialVersionUID = -6800154291154659621L;
	
	/**List of File's to be Extracted*/
	protected final ArrayList<String> filenames;
	protected final Map<String, String[]> replacements;
	
	/**
	 * Builds a Config File from the Given Directory where archives.cfg exists.
	 * @param jarDir
	 */
	private Config(File jarDir){
		super(jarDir, "archives.cfg");
		filenames = new ArrayList<String>();
		replacements = new HashMap<String, String[]>();
	}
	
	public ArrayList<String> getRegisteredZips(){
		return filenames;
	}
	
	public Map<String, String[]> getReplacements(){
		return replacements;
	}
	
	/**
	 * Populates the Filename List
	 */
	public void parseInfo(){
		Scanner scan;
		
		try{
			scan = new Scanner(this);
			filenames.clear();
			
			while(scan.hasNextLine()){
				String tmp = scan.nextLine();
				String replace = null;
				String[] replaceables = null;
				if(tmp.startsWith("#") || "".equals(tmp.trim())) continue;
				
				if(tmp.contains("$R:")){
					replace = tmp.substring(tmp.indexOf("$R:") + 3).trim();
					tmp = tmp.substring(0, tmp.indexOf("$R:")).trim();
					replaceables = replace.split(",");
				}
				
				filenames.add(tmp);
				if(replaceables != null)
					this.replacements.put(tmp, replaceables);
			}
			
			scan.close();
		}catch(RuntimeException e){
			Console.exception((ConfigException) e);
		}catch(Exception e){
			Console.exception(e);
		}
	}
	
	/**
	 * Generates (Or Re-Generates) the Config file with the Default Information specified in Ref.DEF_CFG
	 */
	public void generate(){
		FileOutputStream fos = null;
		InputStream is = null;
		try{
			byte[] buffer = new byte[8192];
			createNewFile();
			fos = new FileOutputStream(this);
			is = new ByteArrayInputStream(Ref.DEF_CFG.getBytes());
			
			for(int c = is.read(buffer); c > 0; c = is.read(buffer))
				fos.write(buffer, 0, c);
		
			fos.close();
			is.close();
		}catch(IOException e){
			Console.exception(e);
		}finally{
			Utility.closeStream(fos);
			Utility.closeStream(is);
		}
	}
	
	/**
	 * Returns a Working Archives.cfg File, whether or not it had to be generated in its default state.
	 * @return
	 */
	public static Config getConfig(){
		if(LaunchPoint.config != null) return LaunchPoint.config;
		Config file = new Config(Ref.getJarDir());
		
		if(!file.exists()){
			file.generate();
		}
		
		if(!file.exists())
			throw new ConfigException(CEType.GENERATION);
		else{
			file.parseInfo();
		}
		return file;
	}
	
}
