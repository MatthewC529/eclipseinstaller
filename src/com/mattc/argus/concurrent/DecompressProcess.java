package com.mattc.argus.concurrent;

/**
 * Describes a Runnable used in Decompressing an Archive File. <br/>
 * <br />
 * @see ZipProcess
 * 
 * @author Matthew Crocco
 */
public interface DecompressProcess extends Runnable{

	public String getArchiveName();
	public String getDestination();
	
}
