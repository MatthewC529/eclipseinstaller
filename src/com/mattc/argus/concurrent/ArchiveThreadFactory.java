package com.mattc.argus.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadFactory;

import com.mattc.argus.util.Utility;

/**
 * A Custom ThreadFactory that simply differentiates between standard <code>Runnable</code> <br />
 * and <code>DecompressProcess</code>. Standard Runnable's are given default Thread Names. <br />
 * <code>DecompressProcess</code> objects are given a Thread Name dependent on <br />
 * their Archive Name. <br />
 * <br />
 * <code> SimpleRunnable Using Java.zip</code> Thread = [Thread-1] <br/>
 * <code> ZipProcess Using Java.zip</code> Thread = [Java]
 * @author Matthew Crocco
 *
 */
public class ArchiveThreadFactory implements ThreadFactory{

	public Thread newThread(Runnable r) {
		Runnable task = null;
		
		try {
			/*
			 * This is a bit hack-y...
			 * 
			 * Because of how ThreadPooling works the original Runnable/DecompressProcess
			 * is run through several filters. This process of extracting the original Runnable
			 * starts from the Top of the hierarchy, at the Worker Thread that the executor uses.
			 * 
			 * Thanks to polymorphism we KNOW (from the Source Code of ThreadPoolExecutor) that the
			 * 'r' parameter is in reality a ThreadPoolExecutor.Worker object. That Worker is given a
			 * Runnable in the form of a FutureTask (As seen after tracing the calls) which requires
			 * a Callable object. The original runnable is run through a RunnableAdapter which is also
			 * a Callable object. So we grab the FutureTask from the Worker and then the Callable from
			 * the FutureTask (All Reflectively). Now we have a Callable object... but that is an interface..
			 * and only has one method, but we know it IS actually a RunnableAdapter. Looking at RunnableAdapter,
			 * we can grab the "task" field which IS the original Runnable!
			 * 
			 * It is very hack-y and relies heavily on Polymorphism and Reflection but it works...
			 * all to name Threads.
			 * 
			 */
			FutureTask<?> futureTmp = Utility.grabField(r, "firstTask");
			Callable<?> callableTmp = Utility.grabField(futureTmp, "callable");
			task = Utility.grabField(callableTmp, "task");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		if(task != null && (task instanceof DecompressProcess)){
			String name = ((DecompressProcess) task).getArchiveName();
			return new Thread(r, name.substring(0, name.lastIndexOf(".")));
		}else return new Thread(r);
	}

}
