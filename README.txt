=============================================================
|Argus Installer	|	Matthew Crocco (c) 2014 |  GPL v3   |
=============================================================
															|
This application was developed with the intention of 		|
assisting Computer Science teachers and Students install the| 
latest Eclipse IDE (Luna), Java 7 and other applications 	|
compressed into zip archives, defying educational web 		|
blocks. This program is VERY VERY likely to be faster than	|
the default windows zip utility by running on large buffers	|
and multiple threads.										|
															|
This Application is Open Source Software. Licensed under the|
GNU Public License v3. The license can be found in the		|
"COPYING" file provided. The "APACHE_COMMONS_LICENSE"		|
file is distributed since this program uses the Apache		|
Commons Compress library which requires the Apache License. |
															|
The Apache License is NOT Viral but PLEASE read the GPL v3	|
if you plan on editing this software.						|
=============================================================

File Descriptions:

=========================================================================
argus-x.x.x.jar, argus-x.x.x-src.jar, argus-x.x.x-eclipse-files.zip		|
=========================================================================
																		|
*x = Arbitrary Version/Build Number										|
																		|
argus.jar -- The Actual Runnable Java Program							|
argus-src.jar -- The Java .java Files (Source Files)					|
argus-eclipse-files.jar -- The Actual Eclipse Project, easily			|
				    	   importable into Eclipse!						|
																		|
=========================================================================


=========================================================================
Default ZIP Files														|
=========================================================================
																		|
eclipse-sdk.zip -- The Eclipse IDE Install File, Eclipse Luna as of		|
			  1.0.1d													|
																		|
Java.zip --	The Java 7 Binaries. Can be found by adding it to the 		|
		"installed JRE's" in eclipse. You can search for JRE's by		|
		going to "Configure Build Path" -> Click on JRE System			|
		Library -> Edit -> Installed JRE's -> Search -> the				|
		Java.zip install director. The JRE should be found.				|
																		|
Sublime Text.zip -- Sublime Text Editor, a Top Quality Editor			|
																		|
=========================================================================

=========================================================================
archives.cfg	(See "replace directory" after)							|
=========================================================================
																		|
This is the file REQUIRED by the program, if it is missing, a			|
template will be generated to allow the program to run the				|
default, provided zip archives (Eclipse, Sublime Text, Java 7).			|
																		|
To have other archives extracted, simply add the name EXACTLY			|
without the ".zip" suffix. As of version 1 the program ONLY 			|
extracts Zip Archives. If you have another format, extract it and		|
then compress it in Zip format. This means no .tar balls and no			|
.7z archives. 															|
																		|
Also the recommended limit on File Sizes (by Apache Commons)			|
is 4 GB, I recommend at most 3 GB. Even as a buffered					|
extraction it will take a long time and is liable to error at >4 GB		|
archives. Although this is of course faster than the standard			|
windows zip utility.													|
																		|
You can also specify files that NEED a file replaced to work.			|
An example is provided in the template. Eclipse requires				|
the -vm argument to be specified to work. The program has				|
a built in system to generate a replacement eclipse.ini by				|
platform. Simply follow the same format to specify						|
a file for replacement.													|
=========================================================================

=========================================================================
perms.cfg 																|
=========================================================================
																		|
This file is mostly used in Unix based systems. POSIX is a file 		|
permissions management system. Setting the proper permissions is kind 	|
of like running your computer in "Administrator" on windows, but on  	|
a single file. This program will take any relative or absolute paths 	|
in this file, grab the file the path leads to and apply the permissions |
equivalent to "chmod a+x" which is Execute/Read for All. 				|
 																		|
This must be done for the default 4 for Sublime Text and Eclipse to 	|
run properly in a UNIX environment.                                     |
 																		|
=========================================================================

=========================================================================
replace directory														|
=========================================================================
																		|
Location where all replacement files are placed							|
																		|
=========================================================================

=========================================================================
run.sh/run.bat															|
=========================================================================
																		|
An Automatic Run Script. If you do not want to run java -jar then		|
you can run this script or make a shortcut to this script which will	|
run the command for you!												|
																		|
=========================================================================

=========================================================================
LICENSE and COPYING														|
=========================================================================
																		|
APACHE_COMMONS_LICENSE.txt is the copy of the Apache License 			|
since this application uses the Apache Commons Compress Library			|
to achieve reliability and efficiency.									|
																		|
COPYING is the license for THIS SOFTWARE. It is the GNU Public			|
License v3. This is a very copy-left license and the distribution		|
section should be read if you modify the program!						|
=========================================================================