#Argus Installer v1.1.0a

A Simple Java-Based Extractor

Originally Made for a Computer Science class to get around "technical" limitations by installing Eclipse (Luna) and Java 7u65.

It is multi-threaded to extract each zip as fast as possible using buffers (At most a 8 MiB Buffer per ZIP) and some simple Thread logic. (Much Faster Than Windows XP [And Possibly Win7] Zip Program that is provided by default on most High School Computers.

It will not interact with the Server in anyway besides getting a path to your Desktop folder. It MAY have to be extracted to the desktop (or folder on the desktop) to get I/O Privileges.

The provided Archives.cfg (If Not there it will be generated on the first run with the 3 default Zip Files listed) can be modified by adding names to the list. Any Zip Files with that name will be extracted to the desktop in its own folder.

If a file is NOT Extracted it is most likely due to unlisted name, unsupported format (Only supports ZIP Format), or misspelled on the list. If none of these apply put in an issue.


When you submit an Issue Ticket PLEASE supply the EInstall Log File in the Directory of the your setup.jar. Or Copy and Paste its contents, otherwise I am just guessing.

##Goals:

- [x] Successfully Implement Multi-Threaded, Buffered Zip-Based Installation
- [x] Implement "Replace" Files (See Eclipse.ini for Example)
- [x] Implement Thread Pooling to Prevent Excessive Memory Use
- [ ] Complete Documentation
- [ ] Support more common Compression Formats (Besides ZIP)
- [ ] Refactor for Overall Better Concurrency
- [ ] A Well-Made Graphical User Interface
- [x] Provide 32-bit and 64-bit packages for Windows and Linux
- [ ] Minor: Provide Packages for Mac and Solaris

###Downloads:

[*Due To File Size, I have moved the download's out of GitHub Releases*](https://www.dropbox.com/sh/p8tc86zreerrr7z/AACUnKCTz8pHlGvDCYjw5f2ma)
